module.exports = {
  type: 'react-component',
  npm: {
    esModules: true,
    umd: {
      global: 'Tile',
      externals: {
        react: 'React'
      }
    }
  }
}
