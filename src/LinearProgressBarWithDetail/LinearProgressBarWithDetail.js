// libs
import React from 'react'
import LinearProgress from 'material-ui/LinearProgress'
import PropTypes from 'prop-types'
import lodash from 'lodash'

// src
import style from './LinearProgressBarWithDetail.css'

const LinearProgressBarWithDetail = props => {
  const { list, displayKey, color, leftLabelKey, clickable, onClick } = props
  const data = list.map((obj, index) => { return { ...obj, count: parseInt(obj.count) } })
  let max = lodash.maxBy(data, (o) => { return parseInt(o.count) })
  max = (max) ? parseInt(max.count) : 100

  const style = {

    'backgroundColor': '#DFE8EB',
    height: '8px',
    borderRadius: '4px'
  }

  return (
    <div>
      <ul className="LinearProgressBarWithDetail">
        {data.map(function (oneItem, index) {
          return (
            <li key={index}>
              <div>
                <span className="display-key" data-toggle="tooltip" data-placement="top"
                      title={oneItem[displayKey]}>{oneItem[displayKey]} </span> <span
                className="float-right">{oneItem[leftLabelKey]}</span>

                {(clickable)?
                  <LinearProgress onClick={() => { onClick(oneItem) }} mode="determinate" color={color}
                                  value={oneItem.count} max={(max === 0) ? 100 : max} style={style} />:
                  <LinearProgress mode="determinate" color={color} value={oneItem.count} max={(max === 0) ? 100 : max}
                                  style={style} />

                }

              </div>
            </li>
          )
        })}
      </ul>
    </div>
  )
}

LinearProgressBarWithDetail.defaultProps = {
  color: '#24aedb',
  leftLabelKey: 'count',
  clickable: false
}

LinearProgressBarWithDetail.propTypes = {
  list: PropTypes.array.isRequired,
  displayKey: PropTypes.string.isRequired,
  color: PropTypes.string,
  leftLabelKey: PropTypes.string,
  clickable: PropTypes.bool,
  onClick: PropTypes.func
}

export default LinearProgressBarWithDetail
