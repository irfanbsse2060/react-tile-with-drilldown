// import React, {Component} from 'react'
//
// export default class extends Component {
//   render() {
//     return <div>
//       <h2>Welcome to React components</h2>
//     </div>
//   }
// }



import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
// import './index.css';
import { Popover, Button } from 'antd';
import Tile from './Tile'
import LinearProgressBarWithDetail from './LinearProgressBarWithDetail'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import PropTypes from 'prop-types'


const drilldown = ()=>{
  const list = [
    {name:'abc.docx',count:25},
    { name: 'abc.docx', count: 25 },
    { name: 'abc.docx', count: 25 },
    { name: 'abc.docx', count: 25 },
    {name: 'abc.docx', count: 25 }]

  return (

    <div style={{width:'450px',height:'300px'}}>

      <LinearProgressBarWithDetail list={list} popOverProps={{ title: 'LOGIN FAILURE OVERVIEW' }} leftLabelKey={'count'} displayKey={'name'} />
    </div>
  )
}

export default class extends React.Component {
  state = {
    visible: false,
  }
  static childContextTypes = {
    muiTheme: PropTypes.object
  }

  getChildContext() {

    return {
      muiTheme: getMuiTheme()
    }
  }

  render() {
    return (

      <div>

        <Tile
          popOverProps={{ title: 'LOGIN FAILURE OVERVIEW' }}
          showDropdown={true}
          dropdownContent={drilldown}
          counter={20}
          text="Total Login Failure"
          bg_color="set-bgcolor-blue"

        >


        </Tile>
      </div>
    );
  }
}

