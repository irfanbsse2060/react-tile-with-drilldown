// libs
import React from "react";
import style from "./Tile.css";
import { Popover, Button, Icon } from "antd";
import "antd/dist/antd.css";
import PropTypes from "prop-types";

class Tile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  hide = () => {
    this.setState({
      visible: false
    });
  };

  handleVisibleChange = visible => {
    this.setState({ visible });
  };

  render() {
    const {
      counter,
      text,
      bg_color,
      suffix,
      counter2,
      suffix2,
      showDropdown,
      dropdownContent,
      popOverProps,
      tileStyle,
      dropdownIconUrl
    } = this.props;

    return (
      <div className={"tile-content " + bg_color} style={tileStyle}>
        <h3 className="tile-value">
          {counter} {suffix && <span>{suffix}</span>}
          {counter2 || counter2 == 0 ? "/" + counter2 : ""}
          <span>{suffix2}</span>
        </h3>

        <h6 className="tile-title">
          {text}
          {showDropdown && (
            <Popover
              {...popOverProps}
              content={<a onClick={this.hide}>Close</a>}
              trigger="click"
              placement="bottomLeft"
              visible={this.state.visible}
              onVisibleChange={this.handleVisibleChange}
              content={dropdownContent()}
            >
              <span type="primary" style={{ float: "right" }}>
                <Icon type="down" />
              </span>
            </Popover>
          )}
        </h6>
      </div>
    );
  }
}
Tile.defaultProps = {
  showDropdown: false,
  dropdownIconUrl: "/images/icons/down-arrow.png",
  tileStyle: { backgroundColor: "#1FAFDC" }
};

Tile.propTypes = {
  showDropdown: PropTypes.bool,
  dropdownContent: PropTypes.func,
  popOverProps: PropTypes.object,
  tileStyle: PropTypes.object,
  dropdownIconUrl: PropTypes.string,
  counter: PropTypes.number,
  counter2: PropTypes.number,
  suffix: PropTypes.string,
  suffix2: PropTypes.string
};

export default Tile;
